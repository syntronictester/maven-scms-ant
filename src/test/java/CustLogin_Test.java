import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CustLogin_Test {
	
	WebDriver driver;
	
	@BeforeMethod
	public void setup() throws Exception
	{
	driver = new FirefoxDriver();

	    String baseUrl ="https://10.0.100.58:8181/customer/pages/login.xhtml?from=%2Fpages%2Fhome.xhtml";
	    driver.get(baseUrl);
	    
	}
	
	@Test
	public void custTest() throws Exception {
	
		
		  driver.findElement(By.id("email")).clear();
		  driver.findElement(By.id("email")).sendKeys("areenadira@yahoo.com");             //Email input field identification and data entered
		  Thread.sleep(1000);                                               //this is just sleep command to wait for 1000 ms. 
		  
		
		
		  driver.findElement(By.id("password")).clear();
		  driver.findElement(By.id("password")).sendKeys("1234567C");         //Password input field identification and data entered
		  Thread.sleep(1000);  
		  
		  driver.findElement(By.tagName("Button")).click();                 //Login button identification and click it
		  Thread.sleep(1000);                                                 //this is just sleep command to wait for 1000 ms.
		  
		  driver.findElement(By.linkText("LOGOUT")).click(); 
	}
	
	@AfterMethod
	public void tearDown()
	{
		driver.close();
		  driver.quit(); 
	}
	
	

}
